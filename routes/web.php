<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    function () {
        return view('welcome');
    }
);

Route::get(
    '/version',
    function () {
        return [
            'version' => '1.0',
            'status'  => 'active',
        ];
    }
);

Route::post(
    '/calculate',
    function (\Illuminate\Http\Request $request) {
        $a = $request->get('a', 0);
        $b = $request->get('b', 0);

        return [
            'result' => $a + $b,
        ];
    }
);

Route::post(
    '/create',
    function (\Illuminate\Http\Request $request) {
        $num = $request->get('num', 1);
        $min = $request->get('min', 0);
        $max = $request->get('max', 1);

        $nums    = [];
        $inRange = 0;

        for ($i = 0; $i < $num; $i++) {
            $rand = rand(0, 10000) / 10000;

            array_push($nums, $rand);

            if ($min <= $rand && $rand <= $max) {
                $inRange++;
            }
        }

        return [
            'count' => $inRange,
            //            'result' => $nums,
        ];
    }
);

Route::get('/user', '\App\Http\Controllers\UserController@index');
Route::post('/user', '\App\Http\Controllers\UserController@store');
Route::get('/user/{id}', '\App\Http\Controllers\UserController@show');
Route::put('/user/{id}', '\App\Http\Controllers\UserController@update');
Route::delete('/user/{id}', '\App\Http\Controllers\UserController@delete');

Route::get('/task', '\App\Http\Controllers\TaskController@index');
Route::post('/task', '\App\Http\Controllers\TaskController@store');
Route::get('/task/{id}', '\App\Http\Controllers\TaskController@show');
Route::put('/task/{id}', '\App\Http\Controllers\TaskController@update');
Route::delete('/task/{id}', '\App\Http\Controllers\TaskController@delete');
