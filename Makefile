.DEFAULT_GOAL := help
.PHONY: help up down restart test-up test-down test watch

help: ## Show this help (default)
	@echo "Usage: make [command] [args=\"\"]"
	@echo
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

init: ## Initialize whole local development environment
	cp -n .env.dist .env
	$(MAKE) up
	.docker/update.sh

up: ## Start docker environment
	docker-compose up -d

down: ## Shutdown docker environment
	docker-compose down --remove-orphans

restart: down up ## Stop and start docker environment

test-up: up ## Start docker environment for test
	docker-compose -f docker-compose-test.yml up -d

test-down: ## Shutdown docker environment for test
	-docker-compose -f docker-compose-test.yml down

test: test-up ## Start and init test environment and run tests (recieves `args`)
	.docker/artisan.sh config:clear
	.docker/artisan.sh migrate:fresh --seed --env=testing
	$(MAKE) test-run run args="$(args)"

test-run: ## Just run tests without initialization (recieves `args`)
	.docker/codecept.sh run $(args)

watch: ## Run webpack in watch mode
	.docker/build-npm.sh watch
