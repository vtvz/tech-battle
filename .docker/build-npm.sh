#!/bin/bash

# Usage:
# `.docker/build-npm.sh [build-type]`
#
# Where [build-type] can be:
# * `dev` (default)
# * `prod`
# * `stats`
# * `watch` - rebuild on every change

echo "Building assets"

DIR="$( dirname "${BASH_SOURCE[0]}" )"
U=`/bin/bash -c "$DIR/_docker-user.sh"`

docker-compose run --rm ${U} npm "yarn --frozen-lockfile && yarn build-${1-dev}"
