#!/bin/bash

DIR="$( dirname "${BASH_SOURCE[0]}" )"
U=`/bin/bash -c "$DIR/_docker-user.sh"`

set -o xtrace

# Usage:
# input command                => result command
# .docker/bash.sh              => docker-compose exec -u 1000:1000 app bash
# .docker/bash.sh npm          => docker-compose exec -u 1000:1000 npm bash
# .docker/bash.sh npm run      => docker-compose run -u 1000:1000 npm bash
# .docker/bash.sh npm run --rm => docker-compose run --rm -u 1000:1000 npm bash

docker-compose ${METHOD-exec} ${U} ${1-app} ${CMD-bash}
