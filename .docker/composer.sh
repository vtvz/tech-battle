#!/bin/bash

DIR="$( dirname "${BASH_SOURCE[0]}" )"
U=`/bin/bash -c "$DIR/_docker-user.sh"`

docker-compose exec ${U} app composer "$@"
