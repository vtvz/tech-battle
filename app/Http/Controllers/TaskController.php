<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $task = Task::with([]);

        if ($request->has('title')) {
            $task->where('title', 'like', '%' . $request->input('title') . '%')->get();
        }

        if ($request->has('description')) {
            $task->where('description', 'like', '%' . $request->input('description') . '%')->get();
        }

        return $task->get();
    }

    public function store(Request $request)
    {
        $user = new Task();
        $user->fill($request->all());
        $user->save();

        return $user;
    }

    public function show($id)
    {
        return Task::findOrFail($id);
    }

    public function update($id, Request $request)
    {
        $user = Task::findOrFail($id);

        $user->fill($request->all());
        $user->save();

        return $user;
    }

    public function delete($id)
    {
        $user = Task::findOrFail($id);

        $user->delete();
    }
}
