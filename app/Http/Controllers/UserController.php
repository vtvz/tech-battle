<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::with([]);

        if ($request->has('fname')) {
            $user->where('fname', 'like', '%' . $request->input('fname') . '%')->get();
        }

        if ($request->has('lname')) {
            $user->where('lname', 'like', '%' . $request->input('lname') . '%')->get();
        }

        if ($request->has('sex')) {
            $user->where('sex', $request->input('sex'))->get();
        }

        if ($request->has('from')) {
            $user->where('age', '>=', $request->input('from'))->get();
        }

        if ($request->has('to')) {
            $user->where('age', '<=', $request->input('to'))->get();
        }

        if ($request->has('age')) {
            $user->where('age', $request->input('age'))->get();
        }

        return $user->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->save();

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User                $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->fill($request->all());
        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);

        $user->delete();
    }
}
